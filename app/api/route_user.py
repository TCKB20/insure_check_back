from fastapi import APIRouter, HTTPException,status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
import pyotp
from sqlalchemy.orm import Session
from fastapi import Depends
from app.core.security import create_access_token
from app.db.models.user import Users
from app.db.services.send_email import send_email
from app.db.services.user import authenticate_user, delete_user, get_current_user, get_user_email, get_user_id, list_users, update_user

from app.schemas.email import EmailSchema
from app.schemas.otp import OTPRequest, OTPVerify
from app.schemas.user import UserCreate, UserUpdate
from app.db.session import get_db
from app.db.services.user import create_new_user

router = APIRouter()

@router.post("/login")
def login_for_access_token(data: OAuth2PasswordRequestForm = Depends(),db: Session= Depends(get_db)):
    user = authenticate_user(data.username, data.password,db)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
        )
    access_token = create_access_token(
        data={"sub": user.email}
    )
    return {"access_token": access_token, "token_type": "bearer"}

@router.post("/send-otp/")
def send_otp(request: OTPRequest,db: Session= Depends(get_db)):
    secret = pyotp.random_base32()
    totp = pyotp.TOTP(secret)
    otp = totp.now()

    # Enregistrer le secret associé à l'email (dans une base de données en production)
    user_to_update = UserUpdate(otp=secret)
    user_to_send_otp = get_user_email(email=request.email,db=db)
    update_user(user_to_send_otp.id,user_to_update,db)
    
    try:
        email_schema = EmailSchema(email=request.email,subject="OTP",message=otp)
        send_email(email=email_schema)
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Erreur d'envoi de l'email: {e}")

    return {"message": "OTP envoyé avec succès"}

@router.post("/verify-otp/")
def verify_otp(request: OTPVerify,db: Session= Depends(get_db)):
    # Récupérer le secret associé à l'email
    secret = get_user_email(email=request.email,db=db)
    if not secret:
        raise HTTPException(status_code=404, detail="Email non trouvé")

    # Vérifier l'OTP
    totp = pyotp.TOTP(secret.otp)
    if totp.verify(request.otp,valid_window=10):
        return {"message": "OTP vérifié avec succès"}
    else:
        raise HTTPException(status_code=400, detail="OTP invalide")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")

@router.post("/users", status_code=status.HTTP_201_CREATED)
def create_user(user : UserCreate,db: Session = Depends(get_db)):
    user = create_new_user(user=user,db=db)
    return user

@router.get("/user/{id}")
def get_user_by_id(id: int, db: Session=Depends(get_db)):
    user = get_user_id(id=id, db=db)
    if not user:
        raise HTTPException(detail=f"User with ID {id} does not exist.", status_code=status.HTTP_404_NOT_FOUND)
    return user

@router.get("/users")
def get_all_users(db: Session = Depends(get_db)):
    users = list_users(db=db)
    return users

@router.put("/users/{id}")
def user_update(id: str, user_update: UserUpdate, db: Session = Depends(get_db)):
    user = update_user(id=id,db=db,user=user_update)
    return user

@router.delete("/userd/{id}")
def delete_a_user(id: int ,  db: Session = Depends(get_db), current_user: Users = Depends(get_current_user)):
    user = delete_user(id=id,db=db)
    if user.get("error"):
        raise HTTPException(detail=user.get("error"), status_code=status.HTTP_400_BAD_REQUEST)
    return user
