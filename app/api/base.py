from app.db.base_class import Base
from app.db.models.user import Users
from fastapi import APIRouter
from app.api import route_user

api_router = APIRouter()
api_router.include_router(route_user.router, prefix="",tags=["users"])