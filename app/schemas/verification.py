from pydantic import BaseModel
from typing import Optional
from datetime import date, datetime

class VerificationBase(BaseModel):
    user_id: int
    automobile_id: int
    date_of_verification: date
    status: str

class VerificationCreate(VerificationBase):
    pass

class Verification(VerificationBase):
    id: int

    class Config:
        orm_mode: True
