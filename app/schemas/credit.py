from pydantic import BaseModel
from typing import Optional
from datetime import date, datetime

class CreditBase(BaseModel):
    valeur: float
    prix: float

class CreditCreate(CreditBase):
    pass

class Credit(CreditBase):
    id: int

    class Config:
        orm_mode: True
