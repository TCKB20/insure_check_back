from pydantic import BaseModel
from typing import Optional
from datetime import date, datetime

class TransactionBase(BaseModel):
    user_id: int
    credit_id: int
    transaction_date: Optional[datetime] = None
    amount: float
    currency: str
    payment_method: str
    status: str
    transaction_reference: Optional[str] = None

class TransactionCreate(TransactionBase):
    pass

class Transaction(TransactionBase):
    id: int
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode: True
