from pydantic import BaseModel
from typing import Optional
from datetime import date, datetime
from pyotp import random_base32

class UserBase(BaseModel):
    firstname: str
    lastname: str
    phone: Optional[str] = None
    email: str
    department: Optional[str] = None
    title: Optional[str] = None
    role: Optional[str] = None
    rights: Optional[str] = None
    otp: str = random_base32()

class UserCreate(UserBase):
    password: str

class UserUpdate(UserBase):
    firstname: Optional[str] = None
    lastname: Optional[str] = None
    password: Optional[str] = None
    phone: Optional[str] = None
    email: Optional[str] = None
    department: Optional[str] = None
    title: Optional[str] = None
    role: Optional[str] = None
    rights: Optional[str] = None


class User(UserBase):
    id: int
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode: True
