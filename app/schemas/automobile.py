from pydantic import BaseModel
from typing import Optional
from datetime import date, datetime

class AutomobileBase(BaseModel):
    make: str
    name: str
    model: str
    ownerName: Optional[str] = None
    policyNumber: Optional[str] = None
    vinNumber: Optional[str] = None
    expiredDate: Optional[date] = None

class AutomobileCreate(AutomobileBase):
    pass

class Automobile(AutomobileBase):
    id: int

    class Config:
        orm_mode: True
