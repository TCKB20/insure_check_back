from sqlalchemy import Column, BigInteger, DECIMAL
from db.base_class import Base

class Credits(Base):

    id = Column(BigInteger, primary_key=True, index=True)
    valeur = Column(DECIMAL(10, 2), nullable=False)
    prix = Column(DECIMAL(10, 2), nullable=False)
