from sqlalchemy import Column, String, BigInteger, Date
from db.base_class import Base

class Automobiles(Base):

    id = Column(BigInteger, primary_key=True, index=True)
    make = Column(String(50), nullable=False)
    name = Column(String(50), nullable=False)
    model = Column(String(50), nullable=False)
    ownerName = Column(String(100))
    policyNumber = Column(String(100))
    vinNumber = Column(String(100), unique=True)
    expiredDate = Column(Date)
