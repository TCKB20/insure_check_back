from sqlalchemy import Column, String, BigInteger, TIMESTAMP
from sqlalchemy.sql import func
from app.db.base_class import Base

class Users(Base):

    id = Column(BigInteger, primary_key=True, index=True)
    firstname = Column(String(50), nullable=True)
    lastname = Column(String(50), nullable=True)
    password = Column(String(255), nullable=True)
    phone = Column(String(20))
    email = Column(String(100), unique=True, nullable=False)
    department = Column(String(50))
    title = Column(String(50))
    role = Column(String(50))
    rights = Column(String(50))
    otp = Column(String(20))
    created_at = Column(TIMESTAMP, server_default=func.now(), nullable=False)
    updated_at = Column(TIMESTAMP, server_default=func.now(), onupdate=func.now(), nullable=False)
