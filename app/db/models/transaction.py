from sqlalchemy import Column, String, BigInteger, DECIMAL, ForeignKey, TIMESTAMP
from sqlalchemy.sql import func
from db.base_class import Base

class Transactions(Base):

    id = Column(BigInteger, primary_key=True, index=True)
    user_id = Column(BigInteger, ForeignKey('users.id'), nullable=False)
    credit_id = Column(BigInteger, ForeignKey('credits.id'), nullable=False)
    transaction_date = Column(TIMESTAMP, server_default=func.now(), nullable=False)
    amount = Column(DECIMAL(10, 2), nullable=False)
    currency = Column(String(3), nullable=False)
    payment_method = Column(String(50), nullable=False)
    status = Column(String(20), nullable=False)
    transaction_reference = Column(String(100))
    created_at = Column(TIMESTAMP, server_default=func.now(), nullable=False)
    updated_at = Column(TIMESTAMP, server_default=func.now(), onupdate=func.now(), nullable=False)
