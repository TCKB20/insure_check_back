from sqlalchemy import Column, String,BigInteger,Date, ForeignKey
from db.base_class import Base

class Verifications(Base):

    id = Column(BigInteger, primary_key=True, index=True)
    user_id = Column(BigInteger, ForeignKey('users.id'), nullable=False)
    automobile_id = Column(BigInteger, ForeignKey('automobiles.id'), nullable=False)
    date_of_verification = Column(Date, nullable=False)
    status = Column(String(20), nullable=False)
