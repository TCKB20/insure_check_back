from fastapi import Depends, HTTPException,status
from app.core.config import settings
from jose import JWTError, jwt
from sqlalchemy.orm import Session
from app.db.session import get_db
from app.depends.depend import oauth2_scheme
from app.core.security import hash_password, verify_password
from app.schemas.user import UserCreate, UserUpdate
from app.db.models.user import Users

def get_current_user(token: str = Depends(oauth2_scheme), db: Session= Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials"
    )

    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        email: str = payload.get("sub")
        if email is None:
            raise credentials_exception
    except JWTError:
        raise credentials_exception
    user = get_user_email(email=email, db=db)
    if user is None:
        raise credentials_exception
    return user

def authenticate_user(email: str, password: str,db: Session):
    user = get_user_email(email=email,db=db)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user

def create_new_user(user:UserCreate,db:Session):
    user = Users(
        firstname= user.firstname,
        lastname= user.lastname,
        phone= user.phone,
        email= user.email,
        department= user.department,
        title= user.title,
        role= user.role,
        rights=user.rights,
        password = hash_password(user.password),
    )
    db.add(user)
    db.commit()
    db.refresh(user)
    return user

def list_users(db: Session):
    users = db.query(Users).all()
    return users

def get_user_id(id: int, db: Session):
    user = db.query(Users).filter(Users.id == id).first()
    return user

def get_user_email(email:str,db: Session):
    user = db.query(Users).filter(Users.email == email).first()
    return user

def update_user(id:int, user:UserUpdate, db:Session):
    db_user = db.query(Users).filter(Users.id == id).first()

    if db_user is None:
        raise HTTPException(status_code=404, detail="User not found")

    for key, value in user.dict(exclude_unset=True).items():
        if key == "password":
            value = hash_password(value)
        setattr(db_user, key, value)

    db.commit()
    db.refresh(db_user)
    return db_user


def delete_user(id:int, db:Session):
    user_in_db = db.query(Users).filter(Users.id==id)
    if not user_in_db.first():
        return {"error": f"Could not find user with id {id}"}
    user_in_db.delete()
    db.commit()
    return {"msg":f"deleted user with id {id}"}