import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from typing import List
from pydantic import EmailStr
from jinja2 import Environment, FileSystemLoader
from app.schemas.email import EmailSchema

def send_email(email: EmailSchema):
    sender_email = "ittiqdev28@outlook.com"
    sender_password = "Ittiq2000"
    smtp_server = "smtp.office365.com"
    smtp_port = 587


    msg = MIMEMultipart()
    msg['From'] = sender_email
    msg['To'] = email.email
    msg['Subject'] = email.subject

    env = Environment(loader=FileSystemLoader('templates'))
    template = env.get_template('email_template.html')
    html_content = template.render(message=email.message)
    
    msg.attach(MIMEText(html_content, 'html'))

    try:
        server = smtplib.SMTP(smtp_server, smtp_port)
        server.starttls()
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, email.email, msg.as_string())
        server.quit()
    except Exception as e:
        print(f"Error: {e}")
